# Test case description

This test case consists of a 3D box grid, used to calculate ABL profiles.
More information can be found in the Ph.D. thesis of Tilman Koblitz.

## Grid

The grid consists of 4 x 4 x 48 cells in the two horizontal and normal
directions, respectively. The domain is 40 m wide and 6 km tall.
All side boundaries are periodic, the bottom is a heated rough wall 
boundary and the top is a symmetry boundary.

## Case 1: gabls2

This test case is based on the work of Svensson et al.:
DOI 10.1007/s10546-011-9611-7
A variable wall temperature is set that reflects a diurnal cycle.
The flow is driven by a pressure gradient that is balanced by the Coriolis
force at the top of the domain. Buonyancy forces are active and the
k-epsilon ABL model of Andrey Sogachev et al. (DOI 10.1007/s10546-012-9726-5)
is used, as implemented by Tilman Koblitz and M. Paul van der Laan.
