# Test case description

This test case consists of a 3D box grid, used to simulated atmospheric
surface layer profiles defined by Monin-Obukhov Similarity Theory (MOST)
The non-neutral profiles are in balance with a modified k-epsilon model
that is consistent with MOST, see van der Laan et al: (DOI:x)

## Grid

The grid consists of 64 x 32 x 64 cells in stream-wise (x), lateral (y) 
and normal (z) directions, respectively. The domain is 10 km long, and 
1 km wide and tall. The MOST profiles are set at the inlet (x=0) and at
the top (z=Lz). The side boundaries are symmetry boundaries, at x=Lx an 
outlet is defined, and at z=z0, a rough wall condition is specified.

All test cases starting with ke or ko are simulated with the k-epsilon
turbulence model or k-omega turbulence model, respectively.

## Case 1: ke_neutral

The neutral test case is a simple logarithmic profile, with a roughness 
length of 0.05 m and a reference velocity of 10 m/s at z=50 m.

## Case 2: ke_stable

The stable test case is characterized by a stability parameter of 0.5 
and a reference velocity of 10 m/s at z=50 m.

## Case 3: ke_unstable

The unstable test case is characterized by a stability parameter of 
-0.5 and a reference velocity of 10 m/s at z=50 m.

## Case 4: ke_neutral_z0_box

This test case is the same as Case 1, where a roughness change is placed
at x=1000 m. The roughness change is defined by a roughness box.

## Case 5: ke_neutral_z0_map

This test case is the same as Case 4, where the roughness is defined by
a roughness map.

## Case 6: ko_neutral

This test case is the same as Case 1, using the k-omega model.
